<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* homecontroller class
*/
class categorycontroller extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Activity_model','activity');
  }

  function index()
  {
    $this->load->view('layouts/header');
    $this->load->view('admin/expense_categories/index');
    $this->load->view('layouts/footer');
  }
  function create()
  {
    $this->load->view('layouts/header');
    $this->load->view('admin/expense_categories/create');
    $this->load->view('layouts/footer');
  }
  function store()
  {

    $this->form_validation->set_rules('category','category','required');
    if ($this->form_validation->run() == FALSE)
    {
        $this->load->view('layouts/header');
        $this->load->view('admin/homes/home');
        $this->load->view('layouts/footer');
    }
    else
    {
      $data = array(
        'name' =>$this->input->post('category'),
        'add_date'=>date('Y-m-d - H:i:s'),
        'last_modified'=>date('Y-m-d - H:i:s'),
      );
      $this->activity->store_category($data);
      $this->session->set_flashdata('msg','Data save successfully');
      redirect(base_url('admin/homecontroller'));
    }
  }
  function edit()
  {

  }
  function update()
  {

  }

  function show()
  {

  }
  function delete()
  {

  }
}




?>
