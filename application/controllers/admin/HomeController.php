<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
    /**
     * homecontroller class
     */
    class HomeController extends CI_Controller
    {

      function __construct()
      {
          parent::__construct();
      }
      // load home.php
      public function index()
      {
          $this->load->view('layouts/header');
          $this->load->view('admin/homes/home');
          $this->load->view('layouts/footer');
      }
      // load table.html
      public function table($value='')
      {
        $this->load->view('layouts/header');
        $this->load->view('admin/homes/tables');
        $this->load->view('layouts/footer');

      }
    }





 ?>
