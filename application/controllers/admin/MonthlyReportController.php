<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* homecontroller class
*/
class MonthlyReportController extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {
    $this->load->view('layouts/header');
    $this->load->view('admin/reports/index');
    $this->load->view('layouts/footer');
  }
  function create()
  {
    $this->load->view('layouts/header');
    $this->load->view('admin/reports/create');
    $this->load->view('layouts/footer');
  }
  function store()
  {

  }
  function edit()
  {

  }
  function update()
  {

  }

  function show()
  {

  }
  function delete()
  {

  }
  function test()
  {
    echo "ok";
    $this->load->view('admin/test.php/test');
  }
}





?>
