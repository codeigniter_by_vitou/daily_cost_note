<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* homecontroller class
*/
class OverviewController extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {
    $this->load->view('layouts/header');
    $this->load->view('admin/overviews\/index');
    $this->load->view('layouts/footer');
  }
  function create()
  {
    $this->load->view('layouts/header');
    $this->load->view('admin/expense_categories/create');
    $this->load->view('layouts/footer');
  }
  function store()
  {

  }
  function edit()
  {

  }
  function update()
  {

  }

  function show()
  {

  }
  function delete()
  {

  }
}





?>
