
<div id="content-wrapper">

  <div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
    <!-- DataTables Example -->
    <form id="createForm" action="index.html" method="post">
      <div class="card mb-3">
        <div class="card-header">
          Create</div>
          <div class="card-body">
            <label for="" >Income category<span style="">*</span></label>
            <select id="income" class="col-md-12 col-sm-12 col-xm-12">
              <option value="" selected>Select category</option>
              <option value="" selected>Select category</option>
            </select>
            <label for="name" >Entry date <span>*</span></label>
            <input type="date" name="" value="<?php echo date('Y-m-d');?>" placeholder=""  id="example-date-input">
            <label for="name" >Amount <span>*</span></label>
            <input type="text" name="" value="" placeholder="$0.00">
          </div>
        </div>
        <input type="submit" name="" value="Save" id="submit">
      </form>
      <p class="small text-center text-muted my-5">
        <em>More table examples coming soon...</em>
      </p>

    </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          <span>Copyright © Your Website 2018</span>
        </div>
      </div>
    </footer>

  </div>
  <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>
