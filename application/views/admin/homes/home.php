
<header>
  <div class="col">

  </div>
  <div class="col-8">
    <ul>
      <li class="current" onclick="window.location.href='<?php echo base_url('admin/homecontroller'); ?>'"><span class="brand">H</span>ome</li>
      <li data-target="#exampleModal-addnew" data-toggle="modal">New</li>
      <li data-target="#exampleModal-newCategory" data-toggle="modal">New Category</li>
      <li onclick="window.location.href='<?php echo base_url();?>admin/OverviewController/index'">Overview</li>
    </ul>
  </div>
</header>
<nav>
  <ul>
    <li>
      <a href="#" class="current">Today</a>
      <a href="#" >This weekly</a>
      <a href="#" >This monthly</a>
    </li>
  </ul>
</nav>
<section id="content" class="col-centered">
  <p>Total:
    <span>50 records</span>
  </p>
  <a href="#" id="addnew" data-target="#exampleModal-addnew" data-toggle="modal">Add new</a>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>
          <input type="checkbox" name="" value="">
        </th>
        <th>Amount</th>
        <th>Type</th>
        <th>Category Name</th>
        <th>Date</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <input type="checkbox" name="" value="">
        </td>
        <td>Doe</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
        <td>john@example.com</td>
        <td>
          <a href="#">view </a>
          <span>
            <a href="#"> Edit</a>
          </span>
          <span>
            <a href="#"> Delete</a>
          </span>
        </td>
      </tr>
    </tbody>
  </table>
</section>
<!-- Modal -->
<div class="modal fade" id="exampleModal-addnew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Type</label>
            <select class="aa" name="">
              <option value="">section type</option>
              <option value="">Income</option>
              <option value="">Expense</option>
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Amount:</label>
            <input type="number" placeholder="$0.00" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Category</label>
            <select class="Category" name="">
              <option value="">section category</option>
              <option value="">moto</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- New Category modal -->
<div class="modal fade" id="exampleModal-newCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url('admin/categorycontroller/store')?>" id="categoryForm" role="form" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Name:</label>
            <input type="text" name="category"data-validation="length alphanumeric"
		 data-validation-length="3-12"
		 data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)" placeholder="Enter value" class="form-control" value="<?php echo set_value('category'); ?>">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
