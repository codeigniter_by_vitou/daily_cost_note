<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url()?>assets/sweetalert2/sweetalert2.min.js" charset="utf-8"></script>
<script>
$.validate({
  modules : 'location, date, security, file',
  onModulesLoaded : function() {
    $('#country').suggestCountry();
  }
});

// Restrict presentation length
$('#presentation').restrictLength( $('#pres-max-length') );
$.validate({
  borderColorOnError : '#FFF',
  addValidClassOnAll : true
});
</script>

<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
</script>

<script type="text/javascript">
$.validate();
</script>
<script type="text/javascript">
    <?php if (!empty($this->session->flashdata('msg'))) {?>
       swal({
         position: 'top-end',
         type: 'success',
         title: 'Your work has been saved',
         showConfirmButton: false,
         timer: 1500
       })
     <?php } ?>
</script>

</body>

</html>
