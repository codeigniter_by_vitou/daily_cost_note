
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin - Dashboard</title>
    <!-- Bootstrap core CSS-->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url();?>assets/font-awesome/css/all.min.css" rel="stylesheet" type="text/css">
    <!--admin-style  -->
    <link type="text/css" href="<?php echo base_url();?>assets/css/admin_style.css" rel="stylesheet">
    <!--select2  -->
    <link type="text/css" href="<?php echo base_url();?>assets/css/select2.min.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url();?>assets/sweetalert2/sweetalert2.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
    rel="stylesheet" type="text/css" />


  </head>

  <body >
