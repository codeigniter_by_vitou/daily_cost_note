-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 25, 2018 at 10:14 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `money_calculate`
--

-- --------------------------------------------------------

--
-- Table structure for table `costs`
--

DROP TABLE IF EXISTS `costs`;
CREATE TABLE IF NOT EXISTS `costs` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `amount` varchar(250) NOT NULL,
  `company` varchar(250) NOT NULL,
  `tel1` int(60) NOT NULL,
  `tel2` int(60) NOT NULL,
  `address` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `color` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `add_date` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `costs`
--

INSERT INTO `costs` (`activity_id`, `title`, `type`, `amount`, `company`, `tel1`, `tel2`, `address`, `image`, `color`, `description`, `add_date`, `last_modified`) VALUES
(7, '', '', '10', '', 0, 0, '', '', '', 'food', '2018-09-25 00:00:00', '2018-09-25 09:21:00'),
(6, '', '', '20', '', 0, 0, '', '', '', 'seafood\r\n', '2018-09-25 00:00:00', '2018-09-25 09:20:00'),
(5, '', '', '1000', '', 0, 0, '', '', '', 'food', '2018-09-25 00:00:00', '2018-09-25 09:20:00'),
(8, '', '', '50', '', 0, 0, '', '', '', 'coconut', '2018-09-25 00:00:00', '2018-09-25 09:21:00'),
(9, '', '', '40', '', 0, 0, '', '', '', 'food', '2018-09-25 00:00:00', '2018-09-25 09:22:00'),
(10, '', '', '1', '', 0, 0, '', '', '', 'a', '2018-09-25 00:00:00', '2018-09-25 11:28:00'),
(11, '', '', '10', '', 0, 0, '', '', '', 'food', '2018-09-25 00:00:00', '2018-09-25 11:28:00'),
(12, '', '', '40', '', 0, 0, '', '', '', '10$ for shoe, $10 shirt.', '2018-09-25 00:00:00', '2018-09-25 13:02:00'),
(13, '', '', '15', '', 0, 0, '', '', '', 'for charity', '2018-09-25 00:00:00', '2018-09-25 13:05:00'),
(14, '', '', '20', '', 0, 0, '', '', '', 'give a way', '2018-09-25 00:00:00', '2018-09-25 13:06:00'),
(15, '', '', '14', '', 0, 0, '', '', '', 'give a way', '2018-09-25 00:00:00', '2018-09-25 13:07:00'),
(16, '', '', '1500', '', 0, 0, '', '', '', 'buy zoomer-x', '2018-09-25 00:00:00', '2018-09-25 13:21:00'),
(17, '', '', '50', '', 0, 0, '', '', '', 'buy', '2018-09-25 00:00:00', '2018-09-25 13:59:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
