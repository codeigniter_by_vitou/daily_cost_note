-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 27, 2018 at 10:04 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `expensemanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_category`
--

DROP TABLE IF EXISTS `table_category`;
CREATE TABLE IF NOT EXISTS `table_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `add_date` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_category`
--

INSERT INTO `table_category` (`category_id`, `name`, `add_date`, `last_modified`) VALUES
(18, 'aaaaaaaa', '2018-09-27 17:00:06', '2018-09-27 17:00:06'),
(17, 'aaaa', '2018-09-27 16:54:54', '2018-09-27 16:54:54'),
(16, 'aaaaa', '2018-09-27 16:54:39', '2018-09-27 16:54:39'),
(15, 'aaaaaa', '2018-09-27 16:54:19', '2018-09-27 16:54:19'),
(14, 'aaaa', '2018-09-27 16:53:59', '2018-09-27 16:53:59'),
(13, 'aaaaa', '2018-09-27 16:53:01', '2018-09-27 16:53:01'),
(12, 'aaaaa', '2018-09-27 16:51:59', '2018-09-27 16:51:59'),
(11, 'aaaaa', '2018-09-27 16:51:27', '2018-09-27 16:51:27');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
